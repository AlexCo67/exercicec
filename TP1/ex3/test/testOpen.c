#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

int main()
{
  FILE *fp;
  DIR *FD;
  struct dirent *in_file;

  fp = fopen("test1.txt", "rw");
  printf("yay \n");
  fclose(fp);

  if (NULL == (FD = opendir("test/")))
  {
    printf("ERREUR\n");
    exit(EXIT_FAILURE);
  }

  while ((in_file = readdir(FD)))
  {

    if (!strcmp(in_file->d_name, "."))
      continue;
    if (!strcmp(in_file->d_name, ".."))
      continue;

    printf("bjr\n");
    char *filename = malloc(124 * sizeof(char));
    strcpy(filename, "test/");
    strcat(filename, in_file->d_name);
    fp = fopen(filename, "rw");
    printf("%s \n", filename);

    fclose(fp);

    printf("bjr close\n");
  }

  printf("yay again\n");
  closedir(FD);

  return 0;
}
