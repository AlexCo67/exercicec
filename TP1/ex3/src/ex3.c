/**
 * \file ex3.c
 * \author alexandre COLOMBO
 * \version 0.1
 * \date 2021
 *
 * Basic parsing options skeleton exemple c file.
 */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
#include<fcntl.h>
#include<getopt.h>
#include<unistd.h>
#include<dirent.h>
#include<sys/types.h>
#include<sys/stat.h>

#define STDOUT 1
#define STDERR 2

#define MAX_PATH_LENGTH 4096


#define USAGE_SYNTAX "[OPTIONS] -i INPUT -o OUTPUT"
#define USAGE_PARAMS "OPTIONS:\n\
  -i, --input  INPUT_FILE  : input file\n\
  -o, --output OUTPUT_FILE : output file\n\
***\n\
  -v, --verbose : enable *verbose* mode\n\
  -h, --help    : display this help\n\
"

/**
 * Procedure which displays binary usage
 * by printing on stdout all available options
 *
 * \return void
 */
void print_usage(char* bin_name)
{
  dprintf(1, "USAGE: %s %s\n\n%s\n", bin_name, USAGE_SYNTAX, USAGE_PARAMS);
}


/**
 * Procedure checks if variable must be free
 * (check: ptr != NULL)
 *
 * \param void* to_free pointer to an allocated mem
 * \see man 3 free
 * \return void
 */
void free_if_needed(void* to_free)
{
  if (to_free != NULL) free(to_free);
}


/**
 *
 * \see man 3 strndup
 * \see man 3 perror
 * \return
 */
char* dup_optarg_str()
{
  char* str = NULL;

  if (optarg != NULL)
  {
    str = strndup(optarg, MAX_PATH_LENGTH);

    // Checking if ERRNO is set
    if (str == NULL)
      perror(strerror(errno));
  }

  return str;
}


/**
 * Binary options declaration
 * (must end with {0,0,0,0})
 *
 * \see man 3 getopt_long or getopt
 * \see struct option definition
 */
static struct option binary_opts[] =
{
  { "help",    no_argument,       0, 'h' },
  { "verbose", no_argument,       0, 'v' },
  { "input",   required_argument, 0, 'i' },
  { "output",  required_argument, 0, 'o' },
  { 0,         0,                 0,  0  }
};

/**
 * Binary options string
 * (linked to optionn declaration)
 *
 * \see man 3 getopt_long or getopt
 */
const char* binary_optstr = "hvi:o:";





/**
 * Binary main loop
 *
 * \return 1 if it exit successfully
 */
int main(int argc, char** argv)
{
  /**
   * Binary variables
   * (could be defined in a structure)
   */
  short int is_verbose_mode = 0;
  char* bin_input_param = NULL;
  char* bin_output_param = NULL;

  // Parsing options
  int opt = -1;
  int opt_idx = -1;

  while ((opt = getopt_long(argc, argv, binary_optstr, binary_opts, &opt_idx)) != -1)
  {
    switch (opt)
    {
      case 'i':
        //input param
        if (optarg)
        {
          bin_input_param = dup_optarg_str();
        }
        break;
      case 'o':
        //output param
        if (optarg)
        {
          bin_output_param = dup_optarg_str();
        }
        break;
      case 'v':
        //verbose mode
        is_verbose_mode = 1;
        break;
      case 'h':
        print_usage(argv[0]);

        free_if_needed(bin_input_param);
        free_if_needed(bin_output_param);

        exit(EXIT_SUCCESS);
      default :
        break;
    }
  }

  /**
   * Checking binary requirements
   * (could defined in a separate function)
   */
  if (bin_input_param == NULL)
  {
    dprintf(STDERR, "Bad usage! See HELP [--help|-h]\n");

    // Freeing allocated data
    free_if_needed(bin_input_param);
    // Exiting with a failure ERROR CODE (== 1)
    exit(EXIT_FAILURE);
  }


  // Printing params
  dprintf(1, "** PARAMS **\n%-8s: %s\n%-8s: %s\n%-8s: %d\n",
          "input",   bin_input_param,
          "output",  bin_output_param,
          "verbose", is_verbose_mode);

  // Business logic must be implemented at this point

/*
int inputDesc=open(bin_input_param,O_RDONLY);

 if(inputDesc==-1){
   dprintf(STDERR, "INPUT ADRESS IS NOT CORRECT\n");
   exit(EXIT_FAILURE);
 }

*/

DIR* FD;
struct dirent* in_file;
FILE *entry_file;
char buffer[BUFSIZ];

//if (NULL == (FD=fdopendir (inputDesc)))
if(NULL == (FD=opendir(bin_input_param)))
{
  dprintf(STDERR, "Error, failed to open the directory\n");
  printf("%s\n", strerror(errno));
  exit(EXIT_FAILURE);
}

printf("error or not ? %s\n", strerror(errno));

while ((in_file = readdir(FD)))
{
  printf("in the readdir \n");
    /* On linux/Unix we don't want current and parent directories
     * On windows machine too, thanks Greg Hewgill
     */
    if (!strcmp (in_file->d_name, "."))
        continue;
    if (!strcmp (in_file->d_name, ".."))
        continue;
    /* Open directory entry file for common operation */

    entry_file= fopen("test1.txt","r");
    printf("erreur ? %s\n", strerror(errno));

    entry_file = fopen(in_file->d_name, "rw");
    printf("on read\n");
    if (entry_file == NULL)
    {
        fprintf(stderr, "Error : Failed to open entry file - %s\n", strerror(errno));

        return 1;
    }

    /* Doing some struf with entry_file : */
    /* For example use fgets */
    while (fgets(buffer, BUFSIZ, entry_file) != NULL)
    {
        /* Use fprintf or fwrite to write some stuff into common_file*/
    }

    /* When you finish with the file, close it */
    fclose(entry_file);
}

/* Don't forget to close common file before leaving */

//close(inputDesc);
return 0;



  // Freeing allocated data
  free_if_needed(bin_input_param);




  return EXIT_SUCCESS;
}
