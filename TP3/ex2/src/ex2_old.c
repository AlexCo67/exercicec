/**
 * \file ex2.c
 * \author alexandre COLOMBO
 * \version 0.1
 * \date 2021
 *
 * Basic parsing options skeleton exemple c file.
 */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
#include<fcntl.h>
#include<getopt.h>
#include<unistd.h>
#include<signal.h>

#define STDOUT 1
#define STDERR 2

#define MAX_PATH_LENGTH 4096


#define USAGE_SYNTAX "[OPTIONS] -i INPUT -o OUTPUT"
#define USAGE_PARAMS "OPTIONS:\n\
  -i, --input  INPUT_FILE  : input file\n\
  -o, --output OUTPUT_FILE : output file\n\
***\n\
  -v, --verbose : enable *verbose* mode\n\
  -h, --help    : display this help\n\
"

/**
 * Procedure which displays binary usage
 * by printing on stdout all available options
 *
 * \return void
 */
void print_usage(char* bin_name)
{
  dprintf(1, "USAGE: %s %s\n\n%s\n", bin_name, USAGE_SYNTAX, USAGE_PARAMS);
}


/**
 * Procedure checks if variable must be free
 * (check: ptr != NULL)
 *
 * \param void* to_free pointer to an allocated mem
 * \see man 3 free
 * \return void
 */
void free_if_needed(void* to_free)
{
  if (to_free != NULL) free(to_free);
}


/**
 *
 * \see man 3 strndup
 * \see man 3 perror
 * \return
 */
char* dup_optarg_str()
{
  char* str = NULL;

  if (optarg != NULL)
  {
    str = strndup(optarg, MAX_PATH_LENGTH);

    // Checking if ERRNO is set
    if (str == NULL)
      perror(strerror(errno));
  }

  return str;
}


/**
 * Binary options declaration
 * (must end with {0,0,0,0})
 *
 * \see man 3 getopt_long or getopt
 * \see struct option definition
 */
static struct option binary_opts[] =
{
  { "help",    no_argument,       0, 'h' },
  { "verbose", no_argument,       0, 'v' },
  { "input",   required_argument, 0, 'i' },
  { "output",  required_argument, 0, 'o' },
  { 0,         0,                 0,  0  }
};

/**
 * Binary options string
 * (linked to optionn declaration)
 *
 * \see man 3 getopt_long or getopt
 */
const char* binary_optstr = "hvi:o:";


int GetSizeOfFile(int fileDescriptor){

  int size = lseek (fileDescriptor, 0, SEEK_END);
  lseek(fileDescriptor, 0, SEEK_SET);
  return size;
}


int pidPERE;
int pidFILS;
sigset_t sig_procUSR1;
struct sigaction actionUSR1;
sigset_t sig_procUSR2;
struct sigaction actionUSR2;


void sig_hanUSR1(int sig){

  printf("Signal USR1 reçu ! \n");
  //kill(getppid(),SIGUSR1);
}

void sig_hanUSR2(int sig){
  printf("Signal USR2 reçu ! \n");
  //kill(pidFILS,SIGUSR2);
}


/**
 * Binary main loop
 *
 * \return 1 if it exit successfully
 */
int main(int argc, char** argv)
{
  /**
   * Binary variables
   * (could be defined in a structure)
   */
  short int is_verbose_mode = 0;
  char* bin_input_param = NULL;
  char* bin_output_param = NULL;

  // Parsing options
  int opt = -1;
  int opt_idx = -1;

  while ((opt = getopt_long(argc, argv, binary_optstr, binary_opts, &opt_idx)) != -1)
  {
    switch (opt)
    {
      case 'i':
        //input param
        if (optarg)
        {
          bin_input_param = dup_optarg_str();
        }
        break;
      case 'o':
        //output param
        if (optarg)
        {
          bin_output_param = dup_optarg_str();
        }
        break;
      case 'v':
        //verbose mode
        is_verbose_mode = 1;
        break;
      case 'h':
        print_usage(argv[0]);

        free_if_needed(bin_input_param);
        free_if_needed(bin_output_param);

        exit(EXIT_SUCCESS);
      default :
        break;
    }
  }



sigemptyset(&sig_procUSR1);
sigemptyset(&sig_procUSR2);

actionUSR1.sa_mask=sig_procUSR1;
actionUSR1.sa_flags=0;
actionUSR1.sa_handler=sig_hanUSR1;
sigaction(SIGUSR1,&actionUSR1,0);

actionUSR2.sa_mask=sig_procUSR2;
actionUSR2.sa_flags=0;
actionUSR2.sa_handler=sig_hanUSR2;
sigaction(SIGUSR2,&actionUSR2,0);


//pidPERE=getpid();

if((pidFILS = fork())==0){
pidPERE=getppid();
printf("PID PERE : %d \n", pidPERE);
//kill(pidPERE, SIGUSR2);
exit(0);
}

else{
  printf("test signal2\n");
kill(pidFILS, SIGUSR1);
//kill(pidPERE, SIGUSR2);
exit(0);
}

//printf("on a fini \n");
  return EXIT_SUCCESS;
}
