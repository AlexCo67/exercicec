/**
 * \file ex2.c
 * \author alexandre COLOMBO
 * \version 0.1
 * \date 2021
 *
 * Basic parsing options skeleton exemple c file.
 */

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <fcntl.h>
#include <getopt.h>
#include <errno.h>
#include <time.h>

const int scoreLimit = 3;

pid_t parentPID = 0;
pid_t childPID = 0;

int opponentScore = 0;

char matchLost = 0;

void SigHandler(char* processName, int processSIGUSR, pid_t *opponentPID, int receivedSig)
{
    // Reception Check
    char receptionSuccess = (rand() % 100) > 50;
    printf("%s\t: Signal %d reçu ! Score adverse : %d\n", processName, receivedSig, opponentScore);

    // Stop if win
    if (opponentScore >= scoreLimit)
    {
        printf("%s\t: J'ai perdu !\n", processName);
        kill(*opponentPID, SIGTERM);
        matchLost = 1;
    }
    else
    {
        // Temporisation.
        sleep(1);
        // If failed reception, continue game
        if (!receptionSuccess)
        {
            printf("%s\t: J'ai raté, l'adversaire marque un point.\n", processName);
            opponentScore++;
        }
        kill(*opponentPID, processSIGUSR);
        printf("%s\t: Signal %d envoyé !\n", processName, processSIGUSR);
    }
}

void SigUsrDispatcher(int signum)
{
    // 60% failure of reception
    char receptionSuccess = (rand() % 100) > 60;
    switch (signum)
    {
    case SIGUSR1:
        printf("[ Dispatch du signal %d au CHILD ]\n", signum);
        SigHandler("CHILD", SIGUSR2, &parentPID, signum);
        break;
    case SIGUSR2:
        printf("[ Dispatch du signal %d au PARENT ]\n", signum);
        SigHandler("PARENT", SIGUSR1, &childPID, signum);
        break;
    }
}

int main(int argc, char const *argv[])
{
    srand(time(NULL));
    // Paramétrage du set de signaux
    sigset_t procSigSet;
    sigemptyset(&procSigSet);

    // Paramétrage du sigaction pour les signaux SIGUSR
    struct sigaction sigUsrAction;
    sigUsrAction.sa_flags = 0;
    sigUsrAction.sa_mask = procSigSet;
    sigUsrAction.sa_handler = SigUsrDispatcher;
    sigaction(SIGUSR2, &sigUsrAction, 0);
    sigaction(SIGUSR1, &sigUsrAction, 0);

    pid_t forkPID = fork();
    if (forkPID == 0)
    {
        // Traitement processus enfant
        parentPID = getppid();
    }
    else
    {
        // Traitement processus parent
        childPID = forkPID;
        kill(childPID, SIGUSR1);
        printf("PARENT\t: Je commence la partie en envoyant un SIGUSR1 !\n");
    }
    while (!matchLost);
    return 0;
}
