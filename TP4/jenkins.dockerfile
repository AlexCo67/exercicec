FROM tomcat:8.5-jre11-openjdk

# Updating container
WORKDIR /usr/local/tomcat/webapps
RUN apt update && apt upgrade -y
RUN apt install -y openjdk-11-jdk wget
RUN apt clean && apt autoremove

# Downloading Jenkins WAR package
RUN wget https://get.jenkins.io/war-stable/2.303.3/jenkins.war
WORKDIR /usr/local/tomcat/bin
CMD ["catalina.sh", "run"]

# Exposing ports
EXPOSE 8080
EXPOSE 50000
